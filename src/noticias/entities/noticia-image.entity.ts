import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Noticia } from "./noticia.entity";

@Entity()
export class NoticiaImage {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('text')
    url: string;

    @ManyToOne(
        () => Noticia,
        (noticia) => noticia.images,
        {  onDelete: 'CASCADE' }
    )
    noticia: Noticia
}