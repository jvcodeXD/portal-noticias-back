import  {Column, Entity, OneToMany, PrimaryGeneratedColumn} from 'typeorm'
import { NoticiaImage } from './noticia-image.entity';

@Entity()
export class Noticia {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text', {
        unique: true
    })
    title: string;

    @Column({
        type: 'timestamp',
        default: () => "CURRENT_TIMESTAMP"
    })
    date: Date;

    @Column('text')
    place: string;
    
    @Column('text')
    author: string;
    
    @Column('text')
    content: string;

    @OneToMany(
        () => NoticiaImage,
        (noticiaImage) => noticiaImage.noticia,
        {cascade: true, eager: true}
    )
    images?: NoticiaImage[];

}
