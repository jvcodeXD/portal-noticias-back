import { IsArray, IsDate, IsOptional, IsString } from "class-validator";
import { NoticiaImage } from "../entities";

export class CreateNoticiaDto {

    @IsString()
    title: string;

    @IsString({each: true})
    @IsOptional()
    @IsArray()
    images?: string[];

    @IsString()
    place: string;

    @IsString()
    author: string;

    @IsString()
    content: string;

}
