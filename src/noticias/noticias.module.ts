import { Module } from '@nestjs/common';
import { NoticiasService } from './noticias.service';
import { NoticiasController } from './noticias.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Noticia, NoticiaImage } from './entities';

@Module({
  controllers: [NoticiasController],
  providers: [NoticiasService],
  imports: [
    TypeOrmModule.forFeature([Noticia, NoticiaImage])
  ],
  exports: [
    NoticiasService,
    TypeOrmModule
  ]
})
export class NoticiasModule {}
