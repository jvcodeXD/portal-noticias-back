import { BadRequestException, Injectable, InternalServerErrorException, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { CreateNoticiaDto } from './dto/create-noticia.dto';
import { UpdateNoticiaDto } from './dto/update-noticia.dto';
import { PaginationDto } from '../common/dtos/pagination.dto';
import { Noticia } from './entities/noticia.entity';
import {validate as isUUID} from 'uuid'
import { NoticiaImage } from './entities';

@Injectable()
export class NoticiasService {
  
  private readonly logger = new Logger('NoticiasService')

  constructor (
    @InjectRepository(Noticia)
    private readonly noticiaRepository: Repository<Noticia>,
    
    @InjectRepository(NoticiaImage)
    private readonly noticiaImageRepository: Repository<NoticiaImage>,

    private readonly dataSource: DataSource,
  ){}

  async create(createNoticiaDto: CreateNoticiaDto) {
    try {
      const { images = [], ...noticiaDetails } = createNoticiaDto;
      const noticia = this.noticiaRepository.create({
        ...noticiaDetails,
        images: images.map(image => this.noticiaImageRepository.create({
          url: image
        })),
      });
      await this.noticiaRepository.save(noticia);
      return {...noticia, images}
    }
    catch (error) {
      this.handleDBExceptions(error);
    }
  }

  async findAll(paginationDto: PaginationDto) {
    const {limit = 50, offset = 0} = paginationDto
    const noticias = await this.noticiaRepository.find({
      take: limit,
      skip: offset,
      relations: {
        images: true
      }
    });
    return noticias.map(noticia => ({
      ...noticia,
      images: noticia.images.map(img => img.url)
    }))
  }

  async findOne(term: string) {
    let noticia: Noticia;
    if ((isUUID(term)))
      noticia = await this.noticiaRepository.findOneBy({ id: term });
    else {
      const queryBuilder = this.noticiaRepository.createQueryBuilder('noticia');
      noticia = await queryBuilder
        .where('title =:title or place =:place or author =:author', {
          title: term,
          place: term,
          author: term
        })
        .leftJoinAndSelect('noticia.images', 'noticiaImages')
        .getOne();
    }
    if (!noticia)
      throw new NotFoundException(`Noticia no encontrada`);
    return noticia;
  }

  async find(term: string) {
    let noticias: Noticia[];
    const queryBuilder = this.noticiaRepository.createQueryBuilder('noticia');
    noticias = await queryBuilder
    .where('LOWER(title) LIKE LOWER(:title) OR LOWER(place) LIKE LOWER(:place) OR LOWER(author) LIKE LOWER(:author)', {
      title: `%${term.toLowerCase()}%`,
      place: `%${term.toLowerCase()}%`,
      author: `%${term.toLowerCase()}%`
    })
    .leftJoinAndSelect('noticia.images', 'noticiaImages')
    .getMany();
    if (!noticias)
      throw new NotFoundException(`Noticia no encontrada`);
      const newNoticias = noticias.map((noticia) => ({
        ...noticia,
        images: noticia.images.map((image) => image.url)
      }));
    return newNoticias;
  }

  async findOnePlain( term: string ) {
    const { images = [], ...rest } = await this.findOne( term );
    return {
      ...rest,
      images: images.map( image => image.url )
    }
  }

  async update(id: string, updateNoticiaDto: UpdateNoticiaDto) {

    const {images, ...toUpdate} = updateNoticiaDto

    const noticia = await this.noticiaRepository.preload({id,...toUpdate});
    if (!noticia) throw new NotFoundException("Noticia no encontrada");

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try { 
      if (images) {
        await queryRunner.manager.delete( NoticiaImage, { noticia: { id } });

        noticia.images = images.map( 
          image => this.noticiaImageRepository.create({ url: image }) 
        )
      }
      
      await queryRunner.manager.save( noticia );

      await queryRunner.commitTransaction();
      await queryRunner.release();

      return this.findOnePlain( id );
    }
    catch(error) {
      this.handleDBExceptions(error);
    }
  }

  async remove(id: string) {
    const noticia = await this.findOne(id);
    await this.noticiaRepository.remove(noticia);
  }

  async deleteAllProducts() {
    const query = this.noticiaRepository.createQueryBuilder('noticia');

    try {
      return await query
        .delete()
        .where({})
        .execute();

    } catch (error) {
      this.handleDBExceptions(error);
    }

  }

  private handleDBExceptions(error: any) {
    if (error.code === '23505')
      throw new BadRequestException(error.detail);
    this.logger.error(error);
    throw new InternalServerErrorException('Unexpected error, check server logs');
  }
}
