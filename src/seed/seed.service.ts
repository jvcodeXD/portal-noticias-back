import { Injectable } from '@nestjs/common';
import { NoticiasService } from '../noticias/noticias.service';
import { initialData } from './data/noticia.seed';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SeedService {
  
  constructor(
    private readonly noticiasService: NoticiasService,
    
    @InjectRepository( User )
    private readonly userRepository: Repository<User>
  ){}
  
  async runSeed() {

    await this.insertNewProducts();
    await this.insertUsers();

    return 'SEED EXECUTED';
  }

  private async insertUsers() {
    const seedUsers = initialData.users;
    const users: User[] = [];
    seedUsers.forEach( user => {
      users.push( this.userRepository.create( user ) )
    });
    const dbUsers = await this.userRepository.save( seedUsers )
    return dbUsers[0];
  }

  private async insertNewProducts() {
    await this.noticiasService.deleteAllProducts();
    const products = initialData.noticias;
    const insertPromises = [];
    products.forEach( product => {
      insertPromises.push( this.noticiasService.create( product ) );
    });
    await Promise.all( insertPromises );
    return true;
  }

}
