import * as bcrypt from 'bcrypt';
interface SeedNoticia {
    content: string;
    images: string[];
    title: string;
    author: string;
    place: string;
}

interface SeedUser {
    email: string;
    name: string;
    password: string;
}
interface SeedData {
    noticias: SeedNoticia[];
    users: SeedUser[];
}


export const initialData: SeedData = {
    users: [
        {
            email: "test1@example.com",
            name: "Test One",
            password: bcrypt.hashSync('abc123', 10)
        },
        {
            email: "test2@example.com",
            name: "Test Two",
            password: bcrypt.hashSync('abc123', 10)
        }
    ],
    noticias: [
        {
            title: 'STL en lenguaje C++',
            images: ['cpp01.png', 'cpp02.jpg'],
            author: 'Tony Stark',
            place: 'Oruro, Bolivia',
            content: 'La Standard Template Library (STL) en C++ se destaca como un recurso vital para programadores, ofreciendo herramientas prediseñadas que optimizan la eficiencia y la calidad del código. Contenedores dinámicos como vector y list simplifican la gestión de datos, mientras que algoritmos como sort y find proporcionan soluciones eficientes a tareas comunes. Los iteradores y rangos facilitan la manipulación de secuencias, y la personalización mediante functores y lambdas adapta los algoritmos a necesidades específicas. Con casos de éxito destacados, desde proyectos pequeños hasta empresas líderes, el uso inteligente de la STL demuestra su impacto positivo en la productividad del desarrollo. Para aquellos que buscan aprovechar al máximo la STL, se ofrecen recursos y tutoriales en línea para explorar sus capacidades de manera más profunda. En definitiva, dominar la STL eleva el nivel de programación en C++, permitiendo a los desarrolladores escribir código más eficiente y mantenible.'
        },
        {
            title: 'Dark temporada 3',
            images: ['dark01.jpg','dark02.jpg'],
            author: 'Bruce Banner',
            place: 'La Paz',
            content: 'La tercera y última temporada de la serie alemana "Dark" ha dejado a los fanáticos intrigados y conmovidos. Con el cierre de la trama compleja que involucra viajes en el tiempo, mundos paralelos y destinos entrelazados, los creadores han tejido un desenlace digno que responde a las preguntas clave. Desde la revelación de los secretos familiares hasta los giros inesperados que desafían las leyes del tiempo, la temporada 3 explora conceptos filosóficos y científicos de manera magistral. La evolución de personajes como Jonas, Martha y Claudia añade capas de profundidad a la narrativa. Sin revelar spoilers, los momentos finales proporcionan respuestas impactantes y conclusiones emocionales para los personajes que han fascinado a los seguidores desde el inicio. La serie, reconocida por su complejidad narrativa y su atención al detalle, ha dejado una marca duradera en el género de ciencia ficción. En resumen, la temporada 3 de "Dark" culmina en un brillante cierre que satisfará a los seguidores leales mientras deja espacio para la reflexión sobre los temas explorados a lo largo de la serie.'
        },
        {
            title: 'Echo nueva serie de Marvel',
            images: ['echo01.jpg', 'echo02.jpeg', 'echo03.jpg'],
            author: 'Jennifer Walters',
            place: 'New York',
            content: 'Marvel Studios presenta su última creación con el estreno de "Echo", una nueva serie que sigue los pasos del personaje Maya Lopez, interpretado por Alaqua Cox, conocido por los seguidores de "Hawkeye". La trama se sumerge en el pasado de Maya, destacando sus habilidades de lucha y su capacidad única para imitar movimientos. La serie promete una combinación de acción intensa y una exploración profunda del universo Marvel. Con conexiones potenciales con otros personajes del MCU, los fanáticos están ansiosos por descubrir cómo "Echo" se integrará en la trama general de Marvel y qué sorpresas les aguardan. La primera temporada, recién lanzada, ha generado entusiasmo y teorías entre los seguidores, consolidando a "Echo" como un tema candente en el mundo de los superhéroes.'
        },
        {
            title: 'Estructura de Datos: Importancia en la Programacion',
            images: ['edd01.png','edd02.png','edd03.png','edd04.png','edd05.png'],
            author: 'Tony Stark',
            place: 'Bolivia',
            content: 'La estructura de datos es un pilar fundamental en el arte de la programación. Su importancia radica en su capacidad para organizar y almacenar datos de manera eficiente, facilitando operaciones y optimizando el rendimiento de los programas. En primer lugar, la elección adecuada de estructuras de datos es esencial para resolver problemas de manera efectiva. Cada estructura tiene sus propias características y ventajas, y la selección correcta puede marcar la diferencia en la eficiencia y complejidad del código. Por ejemplo, arrays son ideales para acceder a elementos por índice, mientras que listas enlazadas son más flexibles para inserciones y eliminaciones. Además, las estructuras de datos permiten la gestión eficiente de la memoria. Al asignar y liberar recursos de manera adecuada, se evitan problemas como fugas de memoria o consumo innecesario de recursos, mejorando la estabilidad y rendimiento del programa. La implementación de algoritmos también depende estrechamente de las estructuras de datos. Una búsqueda eficiente, ordenación rápida o manipulación de grafos eficiente requieren el uso inteligente de estructuras de datos específicas. En resumen, la elección y aplicación adecuada de estructuras de datos son cruciales para la creación de programas robustos y eficientes. La comprensión profunda de estas estructuras empodera a los programadores para abordar problemas de manera elegante, optimizando el uso de recursos y mejorando la calidad del software.'
        },
        {
            title: 'Facultad Nacional de Ingenieria, Inscripciones',
            images: ['fni01.jpg','fni02.jpg','fni03.jpg'],
            author: 'Scott Lang',
            place: 'Oruro - Bolivia',
            content: 'La Facultad Nacional de Ingeniería anuncia la apertura de inscripciones para el próximo periodo académico. Los aspirantes a formar parte de nuestra prestigiosa institución pueden acceder al proceso de inscripción a través de nuestro sitio web oficial. Este periodo ofrece oportunidades emocionantes para aquellos que buscan explorar disciplinas innovadoras y desarrollar habilidades prácticas en el campo de la ingeniería. Los programas académicos de la Facultad Nacional de Ingeniería están diseñados para proporcionar una educación integral, equilibrando la teoría con aplicaciones prácticas. Contamos con un cuerpo docente altamente calificado y una infraestructura moderna que brinda un entorno propicio para el aprendizaje y la investigación. Para participar en el proceso de inscripción, los interesados deben seguir los pasos detallados en nuestro portal web. Además, ofrecemos asesoramiento personalizado para aquellos que deseen obtener más información sobre los programas académicos, requisitos de admisión y posibilidades de becas. La Facultad Nacional de Ingeniería se enorgullece de nutrir a la próxima generación de ingenieros y líderes en tecnología. ¡Únete a nosotros y haz parte de una comunidad académica comprometida con la excelencia y la innovación!'
        },
        {
            title: 'How I Meet Your Mother, la mejor serie de Star+',
            images: ['himym01.jpg','himym02.jpg','himym03.jpg'],
            author: 'Wanda Maximoff',
            place: 'New York',
            content: '"How I Met Your Mother" ha llegado a Star+ para cautivar a los fanáticos con su combinación única de comedia, romance y situaciones hilarantes. La serie, aclamada por su narrativa innovadora, sigue las desventuras de Ted Mosby mientras relata a sus hijos cómo conoció a su madre. Con un elenco carismático, diálogos ingeniosos y momentos icónicos, la serie se ha ganado un lugar especial en los corazones de los espectadores. Star+ ofrece a los suscriptores la oportunidad de revivir todas las temporadas de esta serie atemporal. Desde los legendarios momentos de Barney Stinson hasta las románticas desventuras de Ted, la plataforma brinda acceso a una de las comedias más queridas de la televisión. La llegada de "How I Met Your Mother" a Star+ promete llevar a los espectadores en un viaje nostálgico lleno de risas y emociones. Con su combinación única de humor inteligente y conexiones emocionales, la serie sigue siendo una opción inigualable para aquellos que buscan entretenimiento de calidad.'
        },
        {
            title: 'House Of Dragon: Estreno verano 2024',
            images: ['hod01.jpg','hod02.jpg','hod03.jpg'],
            author: 'Scott Lang',
            place: 'Westeros',
            content: 'La anticipada serie "House of Dragon" está lista para cautivar a los fanáticos con su estreno programado para el verano de 2024. Esta producción, derivada del exitoso universo de "Game of Thrones", promete sumergir a los espectadores en una nueva época de intrigas, poder y dragones. Con un elenco estelar y una trama llena de giros, la serie explorará los eventos previos a la Guerra de los Tronos, revelando los secretos de las casas Targaryen y su lucha por el dominio de los Siete Reinos. Los fanáticos pueden esperar una combinación de escenarios épicos, personajes carismáticos y efectos visuales impresionantes, consolidando "House of Dragon" como un evento televisivo imperdible para el próximo verano, mientras los seguidores del universo de George R.R. Martin aguardan con entusiasmo este nuevo capítulo en la historia de Westeros.'
        },
        {
            title: 'ICPC Mundial',
            images: ['icpc01.jpg','icpc02.png','icpc03.png'],
            author: 'Wanda Maximoff',
            place: 'China',
            content: 'La Competencia Internacional Universitaria de Programación (ICPC) Mundial está a punto de llevarse a cabo, congregando a los cerebros más brillantes de la programación de todo el mundo. Este evento anual, considerado como los "Juegos Olímpicos de la Informática", reunirá a equipos de estudiantes de diversas universidades para enfrentarse en desafíos de programación complejos y creativos. Con la creciente importancia de las habilidades tecnológicas en el mundo actual, la ICPC Mundial se presenta como una plataforma para destacar el talento emergente y fomentar la colaboración entre jóvenes programadores. Los participantes competirán en una intensa jornada para resolver problemas algorítmicos en un tiempo limitado, mostrando no solo su destreza técnica, sino también su capacidad para trabajar bajo presión. La ICPC Mundial promete no solo emocionantes competiciones, sino también la oportunidad de establecer conexiones internacionales y contribuir al avance de la innovación en el campo de la informática.'
        },
        {
            title: 'NestJS framework backend',
            images: ['nest01.png', 'nest02.png'],
            author: 'Jennifer Walters',
            place: 'Oruro - Bolivia',
            content: 'NestJS, el marco (framework) de backend basado en Node.js, continúa ganando popularidad en el mundo del desarrollo web. Con su arquitectura modular y orientada a objetos, NestJS ofrece a los desarrolladores una estructura organizada para construir aplicaciones escalables y mantenibles. Este marco utiliza TypeScript, lo que proporciona tipado estático para evitar errores comunes durante el desarrollo. Su enfoque en la utilización de decoradores y la programación orientada a aspectos simplifica la creación de API RESTful y aplicaciones en tiempo real. Además, la integración sencilla con bibliotecas populares como TypeORM para la manipulación de bases de datos y la compatibilidad con WebSockets para la comunicación en tiempo real hacen de NestJS una opción poderosa y versátil. Empresas de renombre adoptan cada vez más este framework para construir sus aplicaciones, lo que refleja su posición como una opción robusta y eficiente para el desarrollo de backend en el ecosistema Node.js.'
        },
        {
            title: 'NextJS framework frontend',
            images: ['next01.jpg', 'next02.png'],
            author: 'Tony Stark',
            place: 'La Paz - Bolivia',
            content: 'Next.js, el framework de frontend basado en React, sigue destacándose como una opción líder en el desarrollo web moderno. Al aprovechar la potencia de React, Next.js facilita la creación de aplicaciones web rápidas y eficientes al proporcionar funcionalidades como renderizado del lado del servidor (SSR) y generación de páginas estáticas (SSG) de manera nativa. Este enfoque mejora significativamente el rendimiento, la SEO y la experiencia del usuario. Con su estructura de archivos intuitiva, Next.js simplifica el desarrollo, permitiendo una fácil navegación entre páginas y la gestión de rutas dinámicas. Además, la compatibilidad incorporada con TypeScript, la posibilidad de integración con APIs fácilmente, y la gestión de estado sencilla a través de React Context o Redux, consolidan a Next.js como una herramienta versátil y poderosa para el desarrollo frontend. Empresas y desarrolladores individuales adoptan ampliamente este framework para construir aplicaciones web modernas y escalables, lo que refleja su impacto positivo en la comunidad de desarrollo.'
        },
        {
            title: 'Olimpiadas de Ciencia y Tecnologia, Facultad Nacional de Ingenieria',
            images: ['olimpiada01.jpg'],
            author: 'Clint Barton',
            place: 'Oruro - Bolivia',
            content: 'La Facultad Nacional de Ingeniería está a punto de ser el epicentro de las Olimpiadas de Ciencia y Tecnología, un evento que reúne a los estudiantes más brillantes para competir en desafíos que abarcan desde la programación hasta la resolución de problemas ingenieriles. Este año, la competencia se centra en fomentar la innovación y la creatividad, desafiando a los participantes a aplicar sus conocimientos teóricos en situaciones del mundo real. Con pruebas que van desde el diseño de soluciones tecnológicas hasta la resolución de problemas matemáticos avanzados, las Olimpiadas ofrecen una plataforma única para que los futuros ingenieros demuestren sus habilidades y trabajen en equipo. Además, destacados profesionales de la industria participarán como jueces y mentores, brindando a los estudiantes la invaluable oportunidad de aprender de expertos y establecer conexiones clave en el campo. Se espera que las Olimpiadas no solo destaquen el talento estudiantil, sino que también inspiren a la próxima generación de líderes en ciencia y tecnología.'
        },
        {
            title: 'Campaña de esterilizacion, Zoonosis Oruro',
            images: ['pets01.jpg','pets02.jpg','pets03.jpg','pets04.jpg','pets05.jpg'],
            author: 'Scott Lang',
            place: 'Oruro - Bolivia',
            content: 'La campaña de esterilización liderada por Zoonosis en Oruro se presenta como un esfuerzo integral para abordar el problema de la sobrepoblación de animales en la región. Con el objetivo de controlar la reproducción descontrolada, el equipo de Zoonosis se embarcará en una serie de clínicas móviles y puntos fijos de esterilización en toda la ciudad. Esta iniciativa no solo busca controlar la población de animales callejeros, sino también promover la salud y el bienestar de las mascotas de la comunidad. La campaña contará con servicios veterinarios gratuitos para dueños de mascotas de bajos recursos, brindando vacunaciones y chequeos médicos junto con la esterilización. Además, se llevará a cabo una intensa campaña de concientización sobre la importancia de la esterilización en la prevención de enfermedades y el fomento de una convivencia responsable con las mascotas. La Campaña de Esterilización de Zoonosis en Oruro representa un paso crucial hacia el control poblacional y la mejora de la calidad de vida de los animales en la comunidad.'
        },
        {
            title: 'Postgres vs Mysql',
            images: ['pg01.jpg', 'pg02.png', 'pg03.png'],
            author: 'Bruce Banner',
            place: 'Cochabamba - Bolivia',
            content: 'La batalla entre PostgreSQL (Postgres) y MySQL ha sido una constante en el mundo de las bases de datos. Ambos sistemas son sistemas de gestión de bases de datos relacionales (RDBMS) de código abierto y cuentan con comunidades de usuarios activas. PostgreSQL se destaca por su énfasis en la conformidad con los estándares de SQL, su soporte para tipos de datos avanzados y sus capacidades de extensibilidad, que permiten a los usuarios definir sus propias funciones y tipos de datos. Además, PostgreSQL tiene una sólida reputación en entornos empresariales y para aplicaciones que requieren manejo de grandes volúmenes de datos y concurrencia. Por otro lado, MySQL es conocido por su rapidez y rendimiento, especialmente en aplicaciones web. Es fácil de instalar y utilizar, y ha sido ampliamente adoptado en el desarrollo de aplicaciones basadas en web debido a su integración eficiente con lenguajes de programación como PHP. MySQL es una opción popular para proyectos más pequeños y medianos debido a su simplicidad y velocidad de respuesta. La elección entre PostgreSQL y MySQL generalmente depende de las necesidades específicas del proyecto. PostgreSQL se inclina hacia entornos más complejos y empresariales, mientras que MySQL es una opción sólida para aplicaciones web y proyectos más pequeños. Ambos sistemas tienen sus pros y contras, y la elección final dependerá de factores como el rendimiento requerido, la complejidad del proyecto y las preferencias del equipo de desarrollo.'
        },
        {
            title: 'Pilfrut nuevo sabor Pera',
            images: ['pil01.jpg', 'pil02.jpg'],
            author: 'Wanda Maximoff',
            place: 'Cochabamba - Bolivia',
            content: 'Pilfrut sorprende a sus seguidores con el lanzamiento de un nuevo sabor, ¡Pera! La reconocida marca de bebidas, conocida por sus innovadoras combinaciones de frutas, presenta una opción refrescante y deliciosa que promete deleitar los paladares de los amantes de las bebidas naturales. El sabor suave y dulce de la pera se une a la calidad característica de Pilfrut, elaborada con ingredientes frescos y sin conservantes artificiales. Este nuevo lanzamiento no solo amplía la gama de opciones para los consumidores, sino que también resalta el compromiso de Pilfrut con la variedad y la calidad en sus productos. ¡Prepárate para disfrutar de la frescura de la pera con Pilfrut y sumérgete en una experiencia única que fusiona lo natural con lo delicioso!'
        },
        {
            title: 'Programacion Orientada a Objetos',
            images: ['poo01.jpg','poo02.png','poo03.png'],
            author: 'Tony Stark',
            place: 'Westeros',
            content: 'La Programación Orientada a Objetos (POO) es un paradigma de programación que organiza el código alrededor de objetos, instancias de clases que encapsulan datos y comportamientos relacionados. Cada clase actúa como una plantilla para la creación de objetos específicos, definiendo tanto sus atributos como métodos. Este enfoque fomenta la modularidad y el encapsulamiento, ocultando la implementación interna de los objetos y exponiendo solo interfaces relevantes. Además, la herencia permite la creación de nuevas clases basadas en clases existentes, facilitando la reutilización de código y la construcción de jerarquías. La POO es fundamental en el desarrollo de software, ya que modela eficazmente el mundo real, mejora la estructura del código y favorece la mantenibilidad y escalabilidad de los programas.'
        },
        {
            title: 'Numeros primos y donde encontrarlos',
            images: ['primos01.jpg','primos02.jpg'],
            author: 'Natasha Romanov',
            place: 'New York',
            content: 'Los números primos son números naturales mayores que 1 que solo tienen dos divisores: 1 y ellos mismos. Son fundamentales en la teoría de números y desempeñan un papel crucial en criptografía y otros campos de las matemáticas y la informática. Los números primos pueden encontrarse utilizando diversas estrategias. Una de las formas más comunes es mediante la aplicación del "Cribado de Eratóstenes", un algoritmo que permite identificar todos los números primos hasta un cierto límite. Además, hay fuentes en línea que proporcionan listas de números primos conocidos, como la "Base de Datos de Números Primos" (Prime Pages), donde se registran números primos gigantes y se actualiza constantemente con nuevos descubrimientos. También, existen programas y scripts computacionales que pueden generar números primos de manera eficiente. Para encontrar números primos más allá de ciertos límites, los métodos computacionales, como el cribado de números primos grandes o el uso de fórmulas específicas, son esenciales. En resumen, los números primos son ubicuos en las matemáticas y su descubrimiento se realiza mediante algoritmos específicos y recursos matemáticos avanzados.'
        },
        {
            title: 'Torneo Internacional de Puzzle',
            images: ['puzzle01.jpg', 'puzzle02.jpeg'],
            author: 'Scott Lang',
            place: 'Westeros',
            content: 'El Torneo Internacional de Puzzle se perfila como un emocionante evento que reunirá a entusiastas de los rompecabezas de todo el mundo. Con participantes de diversas edades y habilidades, el torneo presentará una amplia variedad de desafíos, desde clásicos crucigramas hasta intrincados rompecabezas tridimensionales. Los competidores se enfrentarán a pruebas de velocidad, resolución estratégica y creatividad en la búsqueda de soluciones. Además, el torneo ofrecerá categorías por edades y niveles de dificultad, garantizando que tanto los novatos como los expertos tengan la oportunidad de participar y disfrutar. Este evento no solo celebrará la diversión y el ingenio de los rompecabezas, sino que también fomentará la camaradería entre los participantes internacionales, promoviendo la cultura del juego mental y la resolución de problemas. Prepárate para sumergirte en el fascinante mundo de los puzzles y poner a prueba tus habilidades en el Torneo Internacional de Puzzle.'
        },
        {
            title: 'Torneo de Robotica para estudiantes de colegio',
            images: ['robotica01.jpg','robotica02.jpg'],
            author: 'Natasha Romanov',
            place: 'Cochabamba - Bolivia',
            content: 'El Torneo de Robótica para estudiantes de colegio está a punto de encender la chispa de la innovación y la creatividad entre los jóvenes ingenieros del futuro. Este emocionante evento reunirá a equipos de estudiantes de diferentes instituciones educativas que competirán en desafíos diseñados para poner a prueba sus habilidades en programación, diseño y construcción de robots. Desde laberintos hasta tareas específicas, los participantes demostrarán la funcionalidad y eficiencia de sus creaciones autónomas. El torneo no solo busca impulsar el interés en la ciencia y la tecnología, sino también fomentar el trabajo en equipo, la resolución de problemas y el pensamiento crítico. Con jueces expertos en robótica y tecnología, este evento no solo será una plataforma de competición, sino también un espacio de aprendizaje invaluable donde los estudiantes podrán compartir ideas, aprender de sus errores y, sobre todo, inspirarse para explorar carreras en STEM (ciencia, tecnología, ingeniería y matemáticas). ¡Prepárate para la emocionante competencia de ingeniería en el Torneo de Robótica para estudiantes de colegio!'
        },
        {
            title: 'Scrum que es?',
            images: ['scrum01.png','scrum02.png','scrum03.png','scrum04.png'],
            author: 'Peter Parker',
            place: 'Tarija - Bolivia',
            content: 'Scrum es un marco de trabajo ágil ampliamente utilizado en el desarrollo de software, caracterizado por su enfoque iterativo e incremental. En Scrum, los proyectos se dividen en sprints, periodos de tiempo predefinidos, durante los cuales se planifica, desarrolla y entrega un conjunto específico de funcionalidades. Los roles clave incluyen al Product Owner, encargado de definir las prioridades del producto, al Scrum Master, facilitador del proceso, y al Equipo de Desarrollo, responsable de ejecutar las tareas. Los eventos principales incluyen la planificación de sprints, reuniones diarias para sincronización, revisiones y retrospectivas para evaluar y mejorar continuamente. Scrum promueve la flexibilidad, la colaboración y la entrega de valor constante, convirtiéndose en una metodología valiosa no solo en el ámbito del desarrollo de software, sino también en diversos sectores empresariales.'
        },
        {
            title: 'Star+ y el nuevo combo con Disney+',
            images: ['star01.png','star02.jpg','star03.jpg'],
            author: 'Tony Stark',
            place: 'Tarija - Bolivia',
            content: 'Star+ presenta su nuevo y emocionante combo con Disney+, ofreciendo a los usuarios una experiencia de entretenimiento completa. Este paquete integrado combina la magia de Disney+ con la diversidad y contenido adulto de Star+, brindando acceso a un extenso catálogo de películas, series, documentales y contenido original. Desde clásicos animados hasta éxitos de taquilla y producciones originales aclamadas, el combo satisface tanto a los amantes de Disney como a aquellos que buscan propuestas más maduras. Con este lanzamiento, los suscriptores pueden disfrutar de un amplio abanico de opciones, desde historias entrañables hasta emocionantes dramas y comedias, todo bajo una sola suscripción. El combo Disney+ y Star+ se presenta como una oferta completa que promete satisfacer a audiencias de todas las edades y preferencias, consolidándose como una opción líder en el competitivo mundo del streaming de contenido.'
        },
        {
            title: 'Estreno de nueva temporada de The Walking Dead',
            images: ['twd01.jpg','twd02.jpg','twd03.png','twd04.jpg'],
            author: 'Clint Barton',
            place: 'Westeros',
            content: 'La anticipación alcanza su punto álgido con el próximo estreno de la nueva temporada de "The Walking Dead". Los fanáticos de la serie apuestan por otra entrega emocionante y llena de suspenso, mientras los sobrevivientes luchan contra hordas de caminantes y enfrentan amenazas cada vez más intensas. Esta temporada promete revelaciones impactantes y giros inesperados, explorando la evolución de los personajes y sus relaciones en un mundo postapocalíptico. Con un elenco talentoso y guiones hábilmente elaborados, "The Walking Dead" continúa siendo un referente del género zombi. Los seguidores aguardan ansiosos para sumergirse en esta nueva entrega, llena de horror, drama y momentos impactantes que dejarán a los fanáticos al borde de sus asientos. ¡Prepárense para el regreso de la serie que ha mantenido a los espectadores cautivados a lo largo de las temporadas!'
        },
    ]
}