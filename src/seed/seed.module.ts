import { Module } from '@nestjs/common';
import { SeedService } from './seed.service';
import { SeedController } from './seed.controller';
import { NoticiasModule } from 'src/noticias/noticias.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  controllers: [SeedController],
  providers: [SeedService],
  imports: [
    NoticiasModule,
    AuthModule
  ]
})
export class SeedModule {}
