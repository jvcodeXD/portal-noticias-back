import { IsEmail, IsString, MaxLength, Min, MinLength } from "class-validator";

export class LoginUserDto {

    @IsString()
    @IsEmail()
    email: string;

    @IsString()
    @MinLength(5)
    @MaxLength(50)
    password: string;

}
