import { Controller, Get, Post, Body, Patch, Param, Delete, UploadedFile, UseInterceptors, BadRequestException, Res } from '@nestjs/common';
import { FilesService } from './files.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { fileFilter, fileNamer } from './helpers';
import { Response } from 'express';
import { ConfigService } from '@nestjs/config';

@Controller('files')
export class FilesController {
  constructor(
    private readonly filesService: FilesService,
    private readonly configService: ConfigService
  ) { }
  
  @Get('noticias/:imageName')
  findImage(
    @Res() res: Response,
    @Param('imageName') imageName: string
  ) {
    const path = this.filesService.getNoticiaImage(imageName);
    res.sendFile(path)
  }

  @Post('noticias')
  @UseInterceptors(FileInterceptor('file', {
    fileFilter: fileFilter,
    storage: diskStorage({
      destination: './static/noticias',
      filename: fileNamer
    })
  }))
  uploadImage(@UploadedFile() file: Express.Multer.File) {
    console.log(file)
    if (!file) throw new BadRequestException('Asegurese de que el archivo sea una imagen')
    const secureUrl = file.filename;

    return { secureUrl };
  }
}
